using System;
using AutoMapper;
using homework_ef.DAL.Entities;
using homework_ef.Common.DTOs.Project;

namespace homework_ef.BLL.MappingProfiles
{
    public sealed class ProjectsProfile : Profile
    {
        public ProjectsProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<CreateProjectDTO, Project>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(p => DateTime.Now));
        }
    }
}