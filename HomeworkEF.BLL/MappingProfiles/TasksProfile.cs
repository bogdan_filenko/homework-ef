using System;
using AutoMapper;
using homework_ef.DAL.Entities;
using homework_ef.Common.DTOs.Task;

namespace homework_ef.BLL.MappingProfiles
{
    public sealed class TasksProfile : Profile
    {
        public TasksProfile()
        {
            CreateMap<Task, TaskDTO>();

            CreateMap<CreateTaskDTO, Task>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now))
                .ForMember(dest => dest.State, src => src.MapFrom(t => 0));
        }
    }
}