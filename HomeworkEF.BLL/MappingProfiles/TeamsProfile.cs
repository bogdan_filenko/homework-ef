using System;
using AutoMapper;
using homework_ef.DAL.Entities;
using homework_ef.Common.DTOs.Team;

namespace homework_ef.BLL.MappingProfiles
{
    public sealed class TeamsProfile : Profile
    {
        public TeamsProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<CreateTeamDTO, Team>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now));
        }
    }
}