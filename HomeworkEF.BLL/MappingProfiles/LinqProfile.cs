using AutoMapper;
using homework_ef.DAL.Entities;
using homework_ef.Common.DTOs.Linq;

namespace homework_ef.BLL.MappingProfiles
{
    public sealed class LinqProfile : Profile
    {
        public LinqProfile()
        {
            CreateMap<Project, ProjectLinqDTO>();

            CreateMap<User, UserLinqDTO>();

            CreateMap<Team, TeamLinqDTO>();

            CreateMap<Task, TaskLinqDTO>();

            CreateMap<TaskLinqDTO, ShortTaskDTO>();

            CreateMap<TeamLinqDTO, ShortTeamDTO>();
        }
    }
}