using System;
using AutoMapper;
using homework_ef.DAL.Entities;
using homework_ef.Common.DTOs.User;

namespace homework_ef.BLL.MappingProfiles
{
    public sealed class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<User, UserDTO>();

            CreateMap<CreateUserDTO, User>()
                .ForMember(dest => dest.RegisteredAt, src => src.MapFrom(u => DateTime.Now));
        }
    }
}