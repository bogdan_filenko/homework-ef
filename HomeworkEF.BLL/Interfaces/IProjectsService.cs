using System.Collections.Generic;
using System.Threading.Tasks;
using homework_ef.Common.DTOs.Project;

namespace homework_ef.BLL.Interfaces
{
    public interface IProjectsService
    {
        Task<IEnumerable<ProjectDTO>> GetProjects();
        Task<ProjectDTO> GetProjectById(int id);
        Task<ProjectDTO> CreateProject(CreateProjectDTO createDto);
        Task<ProjectDTO> UpdateProject(UpdateProjectDTO updateDto);
        Task DeleteProject(int id);
    }
}