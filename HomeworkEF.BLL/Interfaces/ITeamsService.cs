using System.Threading.Tasks;
using System.Collections.Generic;
using homework_ef.Common.DTOs.Team;

namespace homework_ef.BLL.Interfaces
{
    public interface ITeamsService
    {
        Task<IEnumerable<TeamDTO>> GetTeams();
        Task<TeamDTO> GetTeamById(int id);
        Task<TeamDTO> CreateTeam(CreateTeamDTO createDto);
        Task<TeamDTO> UpdateTeam(UpdateTeamDTO updateDto);
        Task DeleteTeam(int id);
    }
}