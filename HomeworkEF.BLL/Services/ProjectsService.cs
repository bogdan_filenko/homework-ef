using AutoMapper;
using System.Threading.Tasks;
using homework_ef.DAL.Context;
using homework_ef.DAL.Entities;
using homework_ef.BLL.Exceptions;
using homework_ef.BLL.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using homework_ef.Common.DTOs.Project;

namespace homework_ef.BLL.Services
{
    public sealed class ProjectsService : IProjectsService
    {
        private readonly IMapper _mapper;
        private readonly ProjectsContext _db;

        public ProjectsService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            var projectsList = await _db.Projects.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projectsList);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var project = await _db.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);

            if (project == default)
            {
                throw new NoEntityException(typeof(Project), id);
            }

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> CreateProject(CreateProjectDTO createDto)
        {
            if (await _db.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Name == createDto.Name) != default)
            {
                throw new EntityExistsException(typeof(Project), "name", createDto.Name);
            }

            if (await _db.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == createDto.AuthorId) == default)
            {
                throw new NoEntityException(typeof(User), createDto.AuthorId);
            }

            if (await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Id == createDto.TeamId) == default)
            {
                throw new NoEntityException(typeof(Team), createDto.TeamId);
            }

            var newProject = _mapper.Map<Project>(createDto);

            _db.Projects.Add(newProject);
            await _db.SaveChangesAsync();

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public async Task<ProjectDTO> UpdateProject(UpdateProjectDTO updateDto)
        {
            if (await _db.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Name == updateDto.Name) != default)
            {
                throw new EntityExistsException(typeof(Project), "name", updateDto.Name);
            }

            var updatableProjects = await _db.Projects.FirstOrDefaultAsync(p => p.Id == updateDto.Id);

            if (updatableProjects == default)
            {
                throw new NoEntityException(typeof(Project), updateDto.Id);
            }

            updatableProjects.Name = updateDto.Name;
            updatableProjects.Description = updateDto.Description;
            updatableProjects.Deadline = updateDto.Deadline;
            
            await _db.SaveChangesAsync();

            return _mapper.Map<ProjectDTO>(updatableProjects);
        }

        public async System.Threading.Tasks.Task DeleteProject(int id)
        {
            var removableProject = await _db.Projects.FirstOrDefaultAsync(p => p.Id == id);

            if (removableProject == default)
            {
                throw new NoEntityException(typeof(Project), id);
            }

            _db.Projects.Remove(removableProject);
            await _db.SaveChangesAsync();
        }
    }
}