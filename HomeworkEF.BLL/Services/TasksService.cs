using System;
using AutoMapper;
using System.Threading.Tasks;
using homework_ef.DAL.Context;
using homework_ef.DAL.Entities;
using homework_ef.BLL.Exceptions;
using homework_ef.BLL.Interfaces;
using System.Collections.Generic;
using homework_ef.Common.DTOs.Task;
using Microsoft.EntityFrameworkCore;

namespace homework_ef.BLL.Services
{
    public sealed class TasksService : ITasksService
    {
        private const int FINISHED_TASK_CODE = 2;

        private readonly IMapper _mapper;
        private readonly ProjectsContext _db;

        public TasksService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            var tasksList = await _db.Tasks.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasksList);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var task = await _db.Tasks.AsNoTracking().FirstOrDefaultAsync(t => t.Id == id);

            if (task == default)
            {
                throw new NoEntityException(typeof(homework_ef.DAL.Entities.Task), id);
            }
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> CreateTask(CreateTaskDTO createDto)
        {
            if (await _db.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == createDto.PerformerId) == default)
            {
                throw new NoEntityException(typeof(User), createDto.PerformerId);
            }

            if (await _db.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Id == createDto.ProjectId) == default)
            {
                throw new NoEntityException(typeof(Project), createDto.ProjectId);
            }

            var newTask = _mapper.Map<homework_ef.DAL.Entities.Task>(createDto);
            _db.Tasks.Add(newTask);
            await _db.SaveChangesAsync();

            return _mapper.Map<TaskDTO>(newTask);
        }

        public async Task<TaskDTO> UpdateTask(UpdateTaskDTO updateDto)
        {
            var updatableTask = await _db.Tasks.FirstOrDefaultAsync(t => t.Id == updateDto.Id);

            if (updatableTask == default)
            {
                throw new NoEntityException(typeof(homework_ef.DAL.Entities.Task), updateDto.Id);
            }

            if (await _db.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == updateDto.PerformerId) == default)
            {
                throw new NoEntityException(typeof(User), updateDto.PerformerId);
            }
            updatableTask.PerformerId = updateDto.PerformerId;
            updatableTask.Name = updateDto.Name;
            updatableTask.Description = updateDto.Description;
            updatableTask.State = updateDto.State;
            if (updatableTask.State == FINISHED_TASK_CODE)
            {
                updatableTask.FinishedAt = DateTime.Now;
            }

            await _db.SaveChangesAsync();

            return _mapper.Map<TaskDTO>(updatableTask);
        }

        public async System.Threading.Tasks.Task DeleteTask(int id)
        {
            var removableTask = await _db.Tasks.FirstOrDefaultAsync(t => t.Id == id);

            if (removableTask == default)
            {
                throw new NoEntityException(typeof(homework_ef.DAL.Entities.Task), id);
            }

            _db.Tasks.Remove(removableTask);
            await _db.SaveChangesAsync();
        }
    }
}