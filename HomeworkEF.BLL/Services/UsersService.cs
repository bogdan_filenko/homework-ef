using AutoMapper;
using System;
using System.Threading.Tasks;
using homework_ef.DAL.Context;
using homework_ef.DAL.Entities;
using homework_ef.BLL.Interfaces;
using System.Collections.Generic;
using homework_ef.BLL.Exceptions;
using homework_ef.Common.DTOs.User;
using Microsoft.EntityFrameworkCore;

namespace homework_ef.BLL.Services
{
    public sealed class UsersService : IUsersService
    {
        private readonly IMapper _mapper;
        private readonly ProjectsContext _db;

        public UsersService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            var usersList = await _db.Users.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<UserDTO>>(usersList);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var user = await _db.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == id);

            if (user == default)
            {
                throw new NoEntityException(typeof(User), id);
            }
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> CreateUser(CreateUserDTO createDto)
        {
            if (await _db.Users.AsNoTracking().FirstOrDefaultAsync(t => t.Email == createDto.Email) != default)
            {
                throw new EntityExistsException(typeof(User), "email", createDto.Email);
            }

            if (createDto.TeamId.HasValue && await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Id == createDto.TeamId) == default)
            {
                throw new NoEntityException(typeof(Team), createDto.TeamId.Value);
            }

            var newUser = _mapper.Map<User>(createDto);
            _db.Users.Add(newUser);
            await _db.SaveChangesAsync();

            return _mapper.Map<UserDTO>(newUser);
        }

        public async Task<UserDTO> UpdateUser(UpdateUserDTO updateDto)
        {
            if (await _db.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Email == updateDto.Email) != default)
            {
                throw new EntityExistsException(typeof(User), "email", updateDto.Email);
            }

            var updatableUser = await _db.Users.FirstOrDefaultAsync(u => u.Id == updateDto.Id);

            if (updatableUser == default)
            {
                throw new NoEntityException(typeof(User), updateDto.Id);
            }

            if (updateDto.TeamId.HasValue && await _db.Teams.AsNoTracking().FirstOrDefaultAsync(t => t.Id == updateDto.TeamId) == default)
            {
                throw new NoEntityException(typeof(Team), updateDto.TeamId.Value);
            }
            
            updatableUser.TeamId = updateDto.TeamId;
            updatableUser.FirstName = updateDto.FirstName;
            updatableUser.LastName = updateDto.LastName;
            updatableUser.BirthDate = updateDto.BirthDate;
            updatableUser.Email = updateDto.Email;

            await _db.SaveChangesAsync();
            
            return _mapper.Map<UserDTO>(updatableUser);
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            var removableUser = await _db.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (removableUser == default)
            {
                throw new NoEntityException(typeof(User), id);
            }

            _db.Users.Remove(removableUser);
            await _db.SaveChangesAsync();
        }
    }
}