using Microsoft.AspNetCore.Builder;
using homework_ef.WebAPI.Middlewares;

namespace homework_ef.WebAPI.Extensions
{
    public static class MiddlewareExtensions
    {
        public static void UseErrorsMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandleMiddleware>();
        }
    }
}