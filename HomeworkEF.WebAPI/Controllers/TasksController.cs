using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using homework_ef.BLL.Interfaces;
using homework_ef.Common.DTOs.Task;

namespace homework_ef.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;
        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _tasksService.GetTasks());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return Ok(await _tasksService.GetTaskById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] CreateTaskDTO dto)
        {
            return Ok(await _tasksService.CreateTask(dto));
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> Put([FromBody] UpdateTaskDTO dto)
        {
            return Ok(await _tasksService.UpdateTask(dto));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _tasksService.DeleteTask(id);

            return NoContent();
        }
    }
}