using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using homework_ef.BLL.Interfaces;
using homework_ef.Common.DTOs.Project;

namespace homework_ef.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _projectsService.GetProjects());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return Ok(await _projectsService.GetProjectById(id));
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] CreateProjectDTO dto)
        {
            return Ok(await _projectsService.CreateProject(dto));
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> Put([FromBody] UpdateProjectDTO dto)
        {
            return Ok(await _projectsService.UpdateProject(dto));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _projectsService.DeleteProject(id);

            return NoContent();
        }
    }
}