#nullable enable

namespace homework_ef.Common.DTOs.Linq
{
    public sealed class ShortTaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}";
        }
    }
}