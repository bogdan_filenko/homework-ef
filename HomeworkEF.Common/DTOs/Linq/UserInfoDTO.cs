using System.Text;

namespace homework_ef.Common.DTOs.Linq
{
    public sealed class UserInfoDTO
    {
        public UserLinqDTO User { get; set; }
        public ProjectLinqDTO LastProject { get; set; }
        public int LastProjectTotalTasks { get; set; }
        public int TotalUnfinishedTasks { get; set; }
        public TaskLinqDTO LongestTask { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();

            viewBuilder.Append("User:\n" +
                                $"\tId: {this.User.Id}\n" +
                                $"\tName: {this.User.FirstName + " " + this.User.LastName}\n");

            if (this.LastProject == default)
            {
                viewBuilder.Append("The last project: None\n" +
                                    "The last project`s tasks: None\n");
            }
            else
            {
                viewBuilder.Append("The last project:\n" +
                    $"\tId: {this.LastProject?.Id ?? 0}\n" +
                    $"\tName: {this.LastProject.Name}\n" +
                    $"The last project`s tasks: {this.LastProjectTotalTasks}\n");
            }
            viewBuilder.Append($"Unfinished tasks: {this.TotalUnfinishedTasks}\n");
            if (this.LongestTask == default)
            {
                viewBuilder.Append("The longest task: None\n");
            }
            else
            {
                viewBuilder.Append("The longest task:\n" +
                    $"\tId: {this.LongestTask.Id}\n" +
                    $"\tName: {this.LongestTask.Name}\n");
            }

            return viewBuilder.ToString();
        }
    }
}