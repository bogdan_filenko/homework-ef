using System;

#nullable enable

namespace homework_ef.Common.DTOs.Team
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return "Team:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name ?? "None"}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}";
        }
    }
}