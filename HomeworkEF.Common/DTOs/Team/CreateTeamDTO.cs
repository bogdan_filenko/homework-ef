#nullable enable

namespace homework_ef.Common.DTOs.Team
{
    public sealed class CreateTeamDTO
    {
        public string? Name { get; set; }
    }
}