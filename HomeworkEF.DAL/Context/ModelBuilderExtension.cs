using Bogus;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using homework_ef.DAL.Entities;

namespace homework_ef.DAL.Context
{
    public static class ModelBuilderExtension
    {
        private const int RANDOM_TEAMS_COUNT = 10;
        private const int RANDOM_PROJECTS_COUNT = 10;
        private const int RANDOM_USERS_COUNT = 30;
        private const int RANDOM_TASKS_COUNT = 50;
        public static void CreateModel(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasIndex(p => p.Name)
                    .IsUnique();
            
            modelBuilder.Entity<Project>()
                .Property(p => p.Name)
                    .HasMaxLength(50);
            
            modelBuilder.Entity<Project>()
                .Property(p => p.Description)
                    .HasMaxLength(200);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(u => u.Projects)
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.ClientCascade);

            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                    .HasMaxLength(100);
            
            modelBuilder.Entity<Task>()
                .Property(t => t.Description)
                    .HasMaxLength(200);

            modelBuilder.Entity<Team>()
                .HasIndex(t => t.Name)
                    .IsUnique();

            modelBuilder.Entity<Team>()
                .Property(t => t.Name)
                    .HasMaxLength(100);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                    .IsUnique();

            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                    .HasMaxLength(30);

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                    .HasMaxLength(30);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Members)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(teams, users);
            var tasks = GenerateRandomTasks(projects, users);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        private static ICollection<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamsFake = new Faker<Team>()
                .RuleFor(t => t.Id, f => teamId++)
                .RuleFor(t => t.Name, f => f.Name.JobTitle())
                .RuleFor(t => t.CreatedAt, f => DateTime.Now);

            return teamsFake.Generate(RANDOM_TEAMS_COUNT);
        }

        private static ICollection<User> GenerateRandomUsers(ICollection<Team> teams)
        {
            int userId = 1;

            var usersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.RegisteredAt, f => DateTime.Now)
                .RuleFor(u => u.BirthDate, f => f.Date.Between(DateTime.Parse("01/01/2002"), DateTime.Parse("01/01/2015")))
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id);

            return usersFake.Generate(RANDOM_USERS_COUNT);
        }

        private static ICollection<Project> GenerateRandomProjects(ICollection<Team> teams, ICollection<User> users)
        {
            int projectId = 1;

            var projectsFake = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.Name, f => f.Name.JobTitle())
                .RuleFor(p => p.Description, f=> f.Random.Words())
                .RuleFor(p => p.CreatedAt, f => DateTime.Now)
                .RuleFor(p => p.Deadline, f => f.Date.Between(DateTime.Now, DateTime.Now.AddYears(2)))
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id);
            
            return projectsFake.Generate(RANDOM_PROJECTS_COUNT);
        }

        private static ICollection<Task> GenerateRandomTasks(ICollection<Project> projects, ICollection<User> users)
        {
            int taskId = 1;

            var tasksFake = new Faker<Task>()
                .RuleFor(t => t.Id, f => taskId++)
                .RuleFor(t => t.Name, f => f.Random.Word())
                .RuleFor(t => t.Description, f => f.Random.Words())
                .RuleFor(t => t.State, f => f.Random.Int(0, 3))
                .RuleFor(t => t.CreatedAt, f => DateTime.Now)
                .RuleFor(t => t.FinishedAt, (f, t) => t.State == 2 ? DateTime.Now : null)
                .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(t => t.PerformerId, f => f.PickRandom(users).Id);

            return tasksFake.Generate(RANDOM_TASKS_COUNT);
        }
    }
}