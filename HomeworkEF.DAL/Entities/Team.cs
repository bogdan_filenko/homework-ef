using System;
using System.Collections.Generic;

#nullable enable

namespace homework_ef.DAL.Entities
{
    public sealed class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<Project> Projects { get; private set; }
        public ICollection<User> Members { get; private set; }
    }
}