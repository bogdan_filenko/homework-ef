﻿using System.Threading.Tasks;
using homework_ef.Client.DialogExecutors;
#nullable enable

namespace homework_ef.Client
{
    class Program
    {
        private static readonly Cui _cui = new Cui();

        static async Task Main()
        {
            await new MainDialogExecutor(_cui).Execute();
        }
    }
}
