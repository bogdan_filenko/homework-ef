namespace homework_ef.Client.Models
{
    public enum TaskState : byte
    {
        Added,
        InProcess,
        Finished,
        Refused
    }
}