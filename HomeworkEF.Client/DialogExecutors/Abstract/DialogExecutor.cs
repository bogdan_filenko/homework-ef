using System;
using homework_ef.Client.Interfaces;
using homework_ef.Client.Services;
using System.Threading.Tasks;

namespace homework_ef.Client.DialogExecutors.Abstract
{
    public abstract class DialogExecutor : IDisposable
    {
        protected readonly Cui _cui;
        protected bool _isOpened;

        protected readonly ResponseDataViewer _dataViewer;
        protected IHttpService _httpService;

        public DialogExecutor(Cui cui)
        {
            _cui = cui;
            _isOpened = true;

            _httpService = new HttpService();
            _dataViewer = new ResponseDataViewer();
        }

        public abstract Task Execute();

        public virtual void Dispose()
        {
            _httpService.Dispose();
        }

        protected virtual void ResetScreen()
        {
            _cui.ShowMessage("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}